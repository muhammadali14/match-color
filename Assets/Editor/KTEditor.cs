﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using UnityEditor.iOS.Xcode;
using System.IO;

public class KTEditor {

	static bool enableBitCode = false;
	static bool enableObjCExceptions = true;

	[PostProcessBuild]
	public static void AddMissingKeys (BuildTarget buildTarget, string pathToBuiltProject) {
		if (buildTarget == BuildTarget.iOS) {

			// Get plist
			string plistPath = pathToBuiltProject + "/Info.plist";
			PlistDocument plist = new PlistDocument();
			plist.ReadFromString(File.ReadAllText(plistPath));

			// Get root
			PlistElementDict rootDict = plist.root;

			var calenderKey = "NSCalendarsUsageDescription";
			rootDict.SetString(calenderKey,"Welcome");

			var cameraKey = "NSCameraUsageDescription";
			rootDict.SetString(cameraKey,"Welcome");

            //var applovinSDKKey = "AppLovinSdkKey";
            //rootDict.SetString(applovinSDKKey,AdManager.applovinSDKKey);

			// Write to file
			File.WriteAllText(plistPath, plist.WriteToString());

			string projectPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";

			PBXProject pbxProject = new PBXProject();
			pbxProject.ReadFromFile(projectPath);

			string target = pbxProject.TargetGuidByName("Unity-iPhone");    

			if (enableBitCode) {
				pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "YES");
			}
			else {
				pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");
			}

			if (enableObjCExceptions) {
				pbxProject.SetBuildProperty(target, "GCC_ENABLE_OBJC_EXCEPTIONS", "YES");
			}
			else {
				pbxProject.SetBuildProperty(target, "GCC_ENABLE_OBJC_EXCEPTIONS", "NO");
			}

            pbxProject.AddBuildProperty(target,"OTHER_LDFLAGS","-ObjC");

			pbxProject.AddFrameworkToProject(target, "iAd.framework", false);

			pbxProject.WriteToFile (projectPath);
		}
	}
}
