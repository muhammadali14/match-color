﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DeletePlayerPrefs : MonoBehaviour {
    [MenuItem("ResetGame/DeleteAllKeys")]
    static void DeleteAllKeys () {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }
}
