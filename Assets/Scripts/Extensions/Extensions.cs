﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Runtime.InteropServices;

public static class Extensions {

    public static T[] SubArray<T> (this T[] data,int index,int length) {
        T[] result = new T[length];
        Array.Copy(data,index,result,0,length);
        return result;
    }

    public static bool ItemsEqual<TSource> (this TSource[] array1,TSource[] array2) {
        if (array1 == null && array2 == null)
            return true;
        if (array1 == null || array2 == null)
            return false;
        return array1.Count() == array2.Count() && !array1.Except(array2).Any();
    }

    public static bool IsSubsetOf<T> (this List<T> coll1,List<T> coll2) {
        bool isSubset = !coll1.Except(coll2).Any();
        return isSubset;
    }

    public static bool CheckEquality<T> (this IEnumerable<T> first,IEnumerable<T> second) {
        var dictionary = new Dictionary<T,int>();

        Action<IEnumerable<T>,int> setCounts = (items,change) =>
        {
            foreach (var item in items) {
                int count;
                // if not found, count will be the default value of 0
                dictionary.TryGetValue(item,out count);
                dictionary[item] = count + change;
            }
        };

        setCounts(first,+1);
        setCounts(second,-1);

        return dictionary.Values.All(value => value == 0);
    }

    public static T[][] ToJaggedArray<T> (this T[,] twoDimensionalArray) {
        int rowsFirstIndex = twoDimensionalArray.GetLowerBound(0);
        int rowsLastIndex = twoDimensionalArray.GetUpperBound(0);
        int numberOfRows = rowsLastIndex + 1;

        int columnsFirstIndex = twoDimensionalArray.GetLowerBound(1);
        int columnsLastIndex = twoDimensionalArray.GetUpperBound(1);
        int numberOfColumns = columnsLastIndex + 1;

        T[][] jaggedArray = new T[numberOfRows][];
        for (int i = rowsFirstIndex; i <= rowsLastIndex; i++) {
            jaggedArray[i] = new T[numberOfColumns];

            for (int j = columnsFirstIndex; j <= columnsLastIndex; j++) {
                jaggedArray[i][j] = twoDimensionalArray[i,j];
            }
        }
        return jaggedArray;
    }
}
