﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Camera))]
#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class ScaleableCamera : MonoBehaviour {

    public float TARGET_WIDTH = 960.0f;
    public float TARGET_HEIGHT = 540.0f;
    public int PIXELS_TO_UNITS = 30; // 1:1 ratio of pixels to units

    private Camera myCamera;

    void Awake () {
        myCamera = GetComponent<Camera>();
        AdjustCamera();
    }

#if UNITY_EDITOR
    private void LateUpdate () {
        AdjustCamera();
    }
#endif

    private void AdjustCamera () {
        if (myCamera == null) {
            myCamera = GetComponent<Camera>();
        }
        float desiredRatio = TARGET_WIDTH / TARGET_HEIGHT;
        float currentRatio = (float)Screen.width / (float)Screen.height;

        if (currentRatio >= desiredRatio) {
            // Our resolution has plenty of width, so we just need to use the height to determine the camera size
            myCamera.orthographicSize = TARGET_HEIGHT / 4 / PIXELS_TO_UNITS;
        }
        else {
            // Our camera needs to zoom out further than just fitting in the height of the image.
            // Determine how much bigger it needs to be, then apply that to our original algorithm.
            float differenceInSize = desiredRatio / currentRatio;
            myCamera.orthographicSize = TARGET_HEIGHT / 4 / PIXELS_TO_UNITS * differenceInSize;
        }
    }
}