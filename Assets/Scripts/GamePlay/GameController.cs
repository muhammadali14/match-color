﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using DG.Tweening;

public enum BlockType {
    kFilled,
    kEmpty
}

public enum MoveType {
    kStatic,
    kHorizontal,
    kVertical
}

[System.Serializable]
public class DifficultyLevel {
    public int targetScore;
    public int cameraSizeIncrease;

    public float ComboCreationRate {
        set;
        get;
    }

    public float ComboSpeed {
        get;
        set;
    }

    public RowData[] rows;
}

[System.Serializable]
public class RowData {
    public ColumnData[] columns;
}

[System.Serializable]
public class ColumnData {
    public BlockType blockType;
    public MoveType moveType;
    public float moveMagnitude;
}

public class ColorData {
    public Color[,] colors;

    public ColorData (Color[,] colors) {
        IsUsed = false;
        this.colors = colors;
    }

    public bool IsUsed {
        set;
        get;
    }
}

public class GameController : MonoBehaviour {
    public DifficultyLevel[] difficulties;

    //#if UNITY_EDITOR
    //public DifficultyLevel[] debugDifficulties;
    //#endif

    public BlocksSpawner blocksSpawner;

    public Player player;

    public Text scoreText;

    public GameObject gameOverPanel;

    public Color[] colors;

    private static GameController sharedInstance;

    private float speed = 3.0f;
    private float comboCreationRate = 3.0f;

    private int score = 0;

    private int maxDifficultyIndexAchieved = 0;
    private int currentDifficultyIndex = 0;
    private int nextPlayerColorRowToUse = 0;
    private int nextPlayerColorColumnToUse = -1;
    private int lastPlayerColumnUsed = -1;

    private List<ColorData> colorsData = new List<ColorData>();

    private void Awake () {
        sharedInstance = this;
//#if UNITY_EDITOR
        //DifficultyLevel[] updatedDifficulties = new DifficultyLevel[difficulties.Length + debugDifficulties.Length - 1];

        //int filledIndex = 0;

        //for (int i = 0; i < difficulties.Length; i++) {
        //    updatedDifficulties[i] = difficulties[i];
        //    filledIndex++;
        //}

        //int debugIndex = 1;

        //for (int i = filledIndex ; i < updatedDifficulties.Length; i++) {
        //    updatedDifficulties[i] = debugDifficulties[debugIndex];
        //    debugIndex++;
        //}

        //difficulties = updatedDifficulties;

        //difficulties = debugDifficulties;
        //#endif
    }

    private void Start () {
#if !UNITY_EDITOR
        Application.targetFrameRate = 60;
#endif

        DifficultyLevel currentDifficulty = difficulties[currentDifficultyIndex];
        currentDifficulty.ComboSpeed = speed;
        currentDifficulty.ComboCreationRate = comboCreationRate;

        blocksSpawner.Init(currentDifficulty,OnBlockDestroyed);

        scoreText.text = score.ToString();

        StartCoroutine(SetStartingItems());

        player.OnPlayerDeath += OnPlayerDeath;
    }

    private void OnDestroy () {
        if (player) {
            player.OnPlayerDeath -= OnPlayerDeath;
        }
        Destroy(sharedInstance);
    }

    private IEnumerator SetStartingItems () {
        yield return new WaitForEndOfFrame();
        Camera.main.GetComponent<ScaleableCamera>().enabled = false;
        SetPlayerColor(-1);
    }

    private void OnBlockDestroyed (int groupIndex) {
        score += 1;
        scoreText.text = score.ToString();

        DifficultyLevel currentDifficulty = difficulties[currentDifficultyIndex];

        if (score >= currentDifficulty.targetScore || currentDifficulty.targetScore == -1) {
            UpdateDifficulty();
        }

        SetPlayerColor(groupIndex);
    }

    private void UpdateDifficulty () {
        maxDifficultyIndexAchieved++;
        maxDifficultyIndexAchieved = Mathf.Min(maxDifficultyIndexAchieved,difficulties.Length - 1);

        currentDifficultyIndex = maxDifficultyIndexAchieved;

        if (maxDifficultyIndexAchieved > 2) {
            currentDifficultyIndex = Random.Range(2,maxDifficultyIndexAchieved + 1);
        }

        DifficultyLevel currentDifficulty = difficulties[currentDifficultyIndex];

        speed += 0.1f;
        speed = Mathf.Min(speed,5f);

        comboCreationRate -= 0.02f;
        comboCreationRate = Mathf.Max(comboCreationRate,1.0f);

        currentDifficulty.ComboSpeed = speed;
        currentDifficulty.ComboCreationRate = comboCreationRate;

        if (currentDifficultyIndex < maxDifficultyIndexAchieved) {
            currentDifficulty.targetScore = score + 1;
        }

        blocksSpawner.UpdateDifficulty(currentDifficulty);

        Camera.main.DOOrthoSize(Camera.main.orthographicSize + currentDifficulty.cameraSizeIncrease,1.5f).SetEase(Ease.Linear).OnComplete(() => {
        
        });
    }

    private void OnPlayerDeath () {
        blocksSpawner.StopSpawning();
        gameOverPanel.SetActive(true);
    }

    private void SetPlayerColor (int groupIndex) {
        ColorData colorData = colorsData.Where(data => !data.IsUsed).FirstOrDefault();

        if (colorData == null || colorData.IsUsed) {
            DifficultyLevel currentDifficulty = difficulties[currentDifficultyIndex];

            int maxRows = currentDifficulty.rows.Length;
            int maxColumns = currentDifficulty.rows[0].columns.Length;

            BlockType[,] blockTypes = new BlockType[maxRows,maxColumns];
            for (int i = 0; i < blockTypes.GetLength(0); i++) {
                RowData rowData = currentDifficulty.rows[i];
                for (int j = 0; j < blockTypes.GetLength(1); j++) {
                    ColumnData columnData = rowData.columns[j];
                    blockTypes[i,j] = columnData.blockType;
                }
            }

            GetRandomColors(groupIndex + 1,blockTypes);
            SetPlayerColor(groupIndex);
            return;
        }

        int row = nextPlayerColorRowToUse;
        int column = nextPlayerColorColumnToUse;

        System.Random rnd = new System.Random();

        int startingColumnValue = column;

        Color colorToAssign = BlocksSpawner.emptyColor;

        do {
            if (startingColumnValue == -1) {
                do {
                    column = rnd.Next(colorData.colors.GetLength(1));
                } while ((lastPlayerColumnUsed != -1 && lastPlayerColumnUsed == column));
            }
            colorToAssign = colorData.colors[row,column];
            if (colorToAssign == BlocksSpawner.emptyColor) {
                for (row = row + 1; row < colorData.colors.GetLength(0); row++) {
                    colorToAssign = colorData.colors[row,column];
                    if (colorToAssign != BlocksSpawner.emptyColor) {
                        break;
                    }
                }
                if (row >= colorData.colors.GetLength(0)) {
                    row -= 1;
                }
            }

        } while (colorToAssign == BlocksSpawner.emptyColor);

        player.SetColor(colorToAssign);

        if (row + 1 >= colorData.colors.GetLength(0)) {
            SetPropertiesToPickColorFromNextGroup(colorData,column);
        }
        else {
            bool hasIteratedAllColors = false;
            nextPlayerColorRowToUse = row;
            do {
                nextPlayerColorRowToUse++;
                nextPlayerColorColumnToUse = column;

                if (nextPlayerColorRowToUse >= colorData.colors.GetLength(0)) {
                    hasIteratedAllColors = true;
                    break;
                }

            } while (colorData.colors[nextPlayerColorRowToUse,column] == BlocksSpawner.emptyColor);

            if (hasIteratedAllColors) {
                SetPropertiesToPickColorFromNextGroup(colorData,column);
            }
        }
    }

    private void SetPropertiesToPickColorFromNextGroup (ColorData colorData,int column) {
        colorData.IsUsed = true;
        nextPlayerColorRowToUse = 0;
        nextPlayerColorColumnToUse = -1;
        lastPlayerColumnUsed = column;
    }

    private Color[,] GetRandomColors (BlockType[,] blockTypes) {
        System.Random rnd = new System.Random();
        Color[] randomColorsToAssign = colors.OrderBy(c => rnd.Next()).ToArray().SubArray(0,colors.Length);

        Color[,] randomColors = new Color[blockTypes.GetLength(0),blockTypes.GetLength(1)];

        int randomColorIndex = 0;

        for (int i = 0; i < blockTypes.GetLength(0); i++) {
            for (int j = 0; j < blockTypes.GetLength(1); j++) {
                if (blockTypes[i,j] == BlockType.kEmpty) {
                    randomColors[i,j] = BlocksSpawner.emptyColor;
                }
                else {
                    randomColors[i,j] = randomColorsToAssign[randomColorIndex];
                    randomColorIndex++;
                }
            }
        }

        return randomColors;
    }

    private Color[,] GenerateNewColors (int groupIndex,BlockType[,] blockTypes) {
        Color[,] lastColors = null;
        Color[,] randomColors = null;

        if (colorsData.Count > 0 && groupIndex - 1 < colorsData.Count && colorsData[groupIndex - 1] != null) {
            lastColors = colorsData[groupIndex - 1].colors;
            randomColors = GetRandomColors(blockTypes);

            Color[] lastColorsLastRow = lastColors.ToJaggedArray()[lastColors.GetLength(0) - 1];
            lastColorsLastRow = lastColorsLastRow.Where(c => c != BlocksSpawner.emptyColor).ToArray();

            Color[] randomColorsFirstRow = randomColors.ToJaggedArray()[0];
            randomColorsFirstRow = randomColorsFirstRow.Where(c => c != BlocksSpawner.emptyColor).ToArray();

            while (lastColorsLastRow.Intersect(randomColorsFirstRow).Any()) { //if both arrays contain atleast one common element
                randomColors = GetRandomColors(blockTypes);
                randomColorsFirstRow = randomColors.ToJaggedArray()[0];
                randomColorsFirstRow = randomColorsFirstRow.Where(c => c != BlocksSpawner.emptyColor).ToArray();
            }
        }
        else {
            randomColors = sharedInstance.GetRandomColors(blockTypes);
        }

        ColorData data = new ColorData(randomColors);
        sharedInstance.colorsData.Add(data);

        return randomColors;
    }

    public static Color[,] GetRandomColors (int groupIndex,BlockType[,] blockTypes) {
        if (sharedInstance.colorsData.Count > groupIndex) {
            return sharedInstance.colorsData[groupIndex].colors;
        }
        else {
            return sharedInstance.GenerateNewColors(groupIndex,blockTypes);
        }
    }

    public void OnRestartPressed () {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }
}
