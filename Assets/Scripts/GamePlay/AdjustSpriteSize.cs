﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class AdjustSpriteSize : MonoBehaviour {

    public float xScaleMult = 1;
    public float yToXScaleMult = 1;

    private void Start () {
        AdjustSize();
    }

#if UNITY_EDITOR
    private void Update () {
        if (!Application.isPlaying) {
            AdjustSize();
        }
    }
#endif

    private void AdjustSize () {
        // get the sprite width in world space units
        float worldSpriteWidth = GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        float worldSpriteHeight = GetComponent<SpriteRenderer>().sprite.bounds.size.y;

        // get the screen height & width in world space units
        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = (worldScreenHeight / Screen.height) * Screen.width;

        // initialize new scale to the current scale
        Vector3 newScale = transform.localScale;

        // divide screen width by sprite width, set to X axis scale
        newScale.x = (worldScreenWidth * xScaleMult) / worldSpriteWidth;
        newScale.y = newScale.x * yToXScaleMult;
        newScale.z = 1.0f;

        // apply scale change
        transform.localScale = newScale;
    }

    public void UpdateXScale (float xScale) {
        xScaleMult = xScale;
        AdjustSize();
    }
}
