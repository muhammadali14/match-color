﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour {

    public BlastParticles particlePrefab;

    private Transform myTransform;
    private SpriteRenderer mySpriteRenderer;

    private System.Action<Bullet> OnRemove;

    private const float forceMagnitude = 0.5f;

    private void OnDestroy () {
        OnRemove = null;
    }

    private void LateUpdate () {
        if (myTransform != null) {
            Vector3 viewPortPos = Camera.main.WorldToViewportPoint(myTransform.position);
            if (viewPortPos.x < 0 || viewPortPos.x > 1 || viewPortPos.y < 0 || viewPortPos.y > 1) {
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionEnter2D (Collision2D collision) {
        if (collision.gameObject.CompareTag("Block")) {
            BlastReboundType reboundType = collision.gameObject.GetComponent<ColorBlock>().CheckForBlast(mySpriteRenderer.color);
            if (reboundType == BlastReboundType.kBlast) {
                BlastParticles particles = Instantiate(particlePrefab) as BlastParticles;
                particles.transform.position = transform.position;
                particles.Init(mySpriteRenderer.color,0.25f,2f);
                RemoveSelf();
            }
            else if (reboundType == BlastReboundType.kAlreadyBlasted) {
                RemoveSelf();
            }
        }
    }

    private void RemoveSelf () {
        if (OnRemove != null) {
            OnRemove(this);
        }
        Destroy(gameObject);
    }

    public void Init (Color color,System.Action<Bullet> OnRemove) {
        myTransform = transform;
        ChangeColor(color);

        this.OnRemove = OnRemove;

        Rigidbody2D myRigidbody = GetComponent<Rigidbody2D>();
        myRigidbody.AddForce(transform.up * forceMagnitude,ForceMode2D.Impulse);
    }

    public void ChangeColor (Color color) {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        mySpriteRenderer.color = color;
    }
}
