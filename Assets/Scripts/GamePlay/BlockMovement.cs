﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BlockMovement : MonoBehaviour {

    private const float duration = 1.0f;

    private void OnDestroy () {
        transform.DOKill();
    }

    private IEnumerator StartMovement (MoveType moveType,float magnitude) {
        yield return new WaitForEndOfFrame();

        if (moveType == MoveType.kHorizontal) {
            float startingXPos = transform.localPosition.x;
            transform.localPosition = new Vector3(transform.localPosition.x - magnitude,transform.localPosition.y,transform.localPosition.z);
            transform.DOLocalMoveX(startingXPos + magnitude,duration).SetEase(Ease.Linear).SetLoops(-1,LoopType.Yoyo);
        }
        else if (moveType == MoveType.kVertical) {
            float startingYPos = transform.localPosition.y;
            transform.localPosition = new Vector3(transform.localPosition.x,transform.localPosition.y + magnitude,transform.localPosition.z);
            transform.DOLocalMoveY(startingYPos + magnitude,duration).SetEase(Ease.Linear).SetLoops(-1,LoopType.Yoyo);
        }
    }

    public void Init (MoveType moveType,float magnitude) {
        StartCoroutine(StartMovement(moveType,magnitude));
    }
}
