﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(ParticleSystem))]
public class BlastParticles : MonoBehaviour {

    public void Init (Color color,float fadeOutTime,float destroyDuration) {
        ParticleSystem ps = GetComponent<ParticleSystem>();
        var col = ps.colorOverLifetime;
        col.enabled = true;

        Gradient grad = new Gradient();
        grad.SetKeys(new GradientColorKey[] { new GradientColorKey(color,0.0f),new GradientColorKey(color,1.0f) },new GradientAlphaKey[] { new GradientAlphaKey(1.0f,0.0f),new GradientAlphaKey(0.0f,fadeOutTime) });

        col.color = grad;
        Invoke("Remove",destroyDuration);
    }

    private void Remove () {
        Destroy(gameObject);
    }
}
