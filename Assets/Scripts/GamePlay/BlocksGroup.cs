﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class BlocksGroup : MonoBehaviour {

    private ColorBlock[,] blocks;

    private Transform myTransform;

    private float speed;

    private bool canMove = false;

    private int groupIndex;

    private System.Action<int> OnBlockBlast;

    private void Start () {
        AdjustBlocks();
    }

    private void Update () {
#if UNITY_EDITOR
        if (!Application.isPlaying) {
            AdjustBlocks();
        }
#endif
        if (myTransform != null && canMove) {
            myTransform.Translate(-myTransform.up * Time.deltaTime * speed);
            Vector3 viewPortPos = Camera.main.WorldToViewportPoint(myTransform.position);
            if (viewPortPos.y < -.5f) {
                Destroy(gameObject);
            }
        }

    }

    private void OnDestroy () {
        if (blocks != null) {
            for (int i = 0; i < blocks.GetLength(0); i++) {
                for (int j = 0; j < blocks.GetLength(1); j++) {
                    blocks[i,j].OnBlast -= this.OnBlockDestroyed;
                }
            }
        }

        OnBlockBlast = null;
    }

    private void OnBlockDestroyed (int rowIndex) {

        ColorBlock[][] jaggedBlocks = blocks.ToJaggedArray();
        for (int i = 0; i < jaggedBlocks[rowIndex].Length; i++) {
            jaggedBlocks[rowIndex][i].SetBlasted();
        }

        if (OnBlockBlast != null) {
            OnBlockBlast(groupIndex);
        }
    }

    private void AdjustBlocks () {
        if (blocks != null && blocks.Length > 0) {
            for (int i = 0; i < blocks.GetLength(0); i++) {

                float xPos = 1f / blocks.GetLength(1);
                float increaseVal = xPos;

                xPos *= 0.5f;

                for (int j = 0; j < blocks.GetLength(1); j++) {
                    float targetXPos = Camera.main.ViewportToWorldPoint(new Vector3(xPos,0,0)).x;
                    blocks[i,j].transform.position = new Vector3(targetXPos,blocks[i,j].transform.position.y,blocks[i,j].transform.position.z);
                    xPos += increaseVal;
                }
            }
        }
    }

    public void Init (float speed,ColorBlock[,] blocks,int groupIndex,System.Action<int> OnBlockBlast) {
        myTransform = transform;
        this.blocks = blocks;
        this.groupIndex = groupIndex;

        this.OnBlockBlast = OnBlockBlast;

        for (int i = 0; i < blocks.GetLength(0); i++) {
            for (int j = 0; j < blocks.GetLength(1); j++) {
                blocks[i,j].RowIndex = i;
                blocks[i,j].OnBlast += this.OnBlockDestroyed;
            }
        }

        SetSpeed(speed);
        AdjustBlocks();
        canMove = true;
    }

    public void StopMoving () {
        canMove = false;
    }

    public void SetSpeed (float speed) {
        this.speed = speed;
    }
}
