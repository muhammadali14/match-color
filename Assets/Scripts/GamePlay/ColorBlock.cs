﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlastReboundType {
    kBlast,
    kRebound,
    kAlreadyBlasted
}

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class ColorBlock : MonoBehaviour {

    public BlastParticles blockParticlesPrefab;

    private SpriteRenderer mySpriteRenderer;

    public System.Action<int> OnBlast;

    private bool isAlreadyBlasted = false;

    private void OnDestroy () {
        OnBlast = null;
    }

    public void Init (Color color,MoveType moveType,float magnitude) {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        mySpriteRenderer.color = color;

        if (moveType != MoveType.kStatic) {
            BlockMovement movement = gameObject.AddComponent<BlockMovement>();
            movement.Init(moveType,magnitude);
        }
    }

    public BlastReboundType CheckForBlast (Color color) {
        Vector3 viewPortPos = Camera.main.WorldToViewportPoint(transform.position);
        bool canBlast = (mySpriteRenderer.color == color && viewPortPos.y < 1.1f) ? true : false;

        BlastReboundType reboundType = BlastReboundType.kRebound;

        if (canBlast) {
            if (isAlreadyBlasted) {
                reboundType = BlastReboundType.kAlreadyBlasted;
            }
            else {
                BlastParticles particles = Instantiate(blockParticlesPrefab) as BlastParticles;
                particles.transform.position = transform.position;
                particles.Init(mySpriteRenderer.color,0.5f,4f);
                OnBlast(RowIndex);
                OnBlast = null;
                Destroy(gameObject);
            }
        }
        return reboundType;
    }

    public int RowIndex {
        set;
        private get;
    }

    public void SetBlasted () {
        isAlreadyBlasted = true;
    }
}
