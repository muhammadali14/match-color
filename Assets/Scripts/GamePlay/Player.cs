﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {

    public delegate void PlayerDieDelegate();
    public event PlayerDieDelegate OnPlayerDeath;

    public Transform firePointTransform;

    public Bullet bulletPrefab;

    private Transform myTransform;

    private SpriteRenderer mySpriteRenderer;

    private List<Bullet> bullets = new List<Bullet>();

    private bool isGameOver = false;

    private const float leftBoundaryVal = 0.05f;
    private const float rightBoundaryVal = 0.95f;
    private const float speed = 1.25f;

    private void Start () {
        myTransform = transform;
        mySpriteRenderer = GetComponent<SpriteRenderer>();

        float yPos = Camera.main.ViewportToWorldPoint(new Vector3(0,0.2f,0)).y;
        myTransform.position = new Vector3(myTransform.position.x,yPos,myTransform.position.z);

//#if UNITY_EDITOR
//        GetComponent<Collider2D>().enabled = false;
//#endif

        StartShooting();
    }

    private void OnDestroy () {
        OnPlayerDeath = null;
        CancelInvoke();
    }

    private void LateUpdate () {
        if (!isGameOver) {
            if (SystemInfo.deviceType == DeviceType.Handheld) {
                if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
                    Vector2 deltaPos = Input.GetTouch(0).deltaPosition;
                    myTransform.Translate(new Vector3(deltaPos.x * Time.deltaTime * speed,0,0));
                }
            }
            else {
                myTransform.Translate(new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * speed * 20f,0,0));
            }

            Vector3 viewPortPos = Camera.main.WorldToViewportPoint(myTransform.position);
            viewPortPos.x = Mathf.Clamp(viewPortPos.x,leftBoundaryVal,rightBoundaryVal);
            myTransform.position = new Vector3(Camera.main.ViewportToWorldPoint(viewPortPos).x,myTransform.position.y,myTransform.position.z);
        }
    }

    private void OnCollisionEnter2D (Collision2D collision) {
        if (collision.gameObject.CompareTag("Block")) {
            if (OnPlayerDeath != null) {
                OnPlayerDeath();
            }
            isGameOver = true;
            CancelInvoke("Shoot");
        }
    }

    private void StartShooting () {
        InvokeRepeating("Shoot",0.25f,0.25f);
    }

    private void Shoot () {
        Bullet bullet = Instantiate(bulletPrefab,firePointTransform.position,Quaternion.identity) as Bullet;
        bullet.transform.localScale = bulletPrefab.transform.localScale;
        bullet.Init(mySpriteRenderer.color,((Bullet bulletToDestroy) => {
            bullets.Remove(bulletToDestroy);
        }));

        bullets.Add(bullet);
    }

    public void SetColor (Color color) {
        if (!isGameOver) {
            if (mySpriteRenderer == null) {
                mySpriteRenderer = GetComponent<SpriteRenderer>();
            }
            mySpriteRenderer.color = color;

            for (int i = 0; i < bullets.Count; i++) {
                if (bullets[i] != null) {
                    bullets[i].ChangeColor(color);
                }
            }
        }
    }
}
