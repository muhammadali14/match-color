﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BlocksSpawner : MonoBehaviour {

    public ColorBlock colorBlockPrefab;

    public Color[] colors;

    public static readonly Color emptyColor = Color.white;

    private System.Action<int> OnBlockDestroyed;

    private bool isGameOver = false;

    private int groupsIncIndex = 0;

    private float lastCreationTime = -1;

    private DifficultyLevel currentDifficulty;

    private void OnDestroy () {
        OnBlockDestroyed = null;
        CancelInvoke();
    }

    private void OnBlastBlock (int groupIndex) {
        if (OnBlockDestroyed != null) {
            OnBlockDestroyed(groupIndex);
        }
    }

    private void SpawnBlocks () {
        int maxRows = currentDifficulty.rows.Length;
        int maxColumns = currentDifficulty.rows[0].columns.Length;

        ColorBlock[,] blocks = new ColorBlock[maxRows,maxColumns];

        GameObject combo = new GameObject("Combo");

        float zPos = 0;
        float blockHeight = 0;
        float yPosAdd = 0;

        BlockType[,] blockTypes = new BlockType[maxRows,maxColumns];
        for (int i = 0; i < blockTypes.GetLength(0); i++) {
            RowData row = currentDifficulty.rows[i];
            for (int j = 0; j < blockTypes.GetLength(1); j++) {
                ColumnData column = row.columns[j];
                blockTypes[i,j] = column.blockType;
            }
        }

        Color[,] randomColors = GameController.GetRandomColors(groupsIncIndex,blockTypes);

        for (int i = 0; i < maxRows; i++) {
            for (int j = 0; j < maxColumns; j++) {

                ColumnData column = currentDifficulty.rows[i].columns[j];

                ColorBlock block = Instantiate(colorBlockPrefab,Vector3.zero,Quaternion.identity) as ColorBlock;
                block.transform.SetParent(combo.transform);

                block.Init(randomColors[i,j],column.moveType,column.moveMagnitude);
                block.GetComponent<AdjustSpriteSize>().UpdateXScale(1f / maxColumns);
                blocks[i,j] = block;

                blockHeight = block.GetComponent<SpriteRenderer>().bounds.size.y;

                block.transform.localPosition = new Vector3(block.transform.localPosition.x,block.transform.localPosition.y + yPosAdd,zPos);

                zPos += 0.01f;

                if (column.blockType == BlockType.kEmpty) {
                    block.gameObject.SetActive(false);
                }
            }

            yPosAdd += blockHeight;
        }

        BlocksGroup blocksGroup = combo.AddComponent<BlocksGroup>();
        blocksGroup.Init(currentDifficulty.ComboSpeed,blocks,groupsIncIndex,OnBlastBlock);

        combo.transform.SetParent(transform);
        combo.transform.position = new Vector3(0,Camera.main.ViewportToWorldPoint(new Vector3(0,1.2f,0)).y,0);

        combo.name = "Combo_" + groupsIncIndex;

        groupsIncIndex++;

        lastCreationTime = Time.time;
    }

    public void Init (DifficultyLevel difficulty,System.Action<int> OnBlockDestroyed) {
        currentDifficulty = difficulty;
        this.OnBlockDestroyed = OnBlockDestroyed;
        InvokeRepeating("SpawnBlocks",0,currentDifficulty.ComboCreationRate);
    }

    public void StopSpawning () {
        if (!isGameOver) {
            isGameOver = true;
            lastCreationTime = -1;
            CancelInvoke("SpawnBlocks");
            for (int i = 0; i < transform.childCount; i++) {
                BlocksGroup combo = transform.GetChild(i).GetComponent<BlocksGroup>();
                if (combo) {
                    combo.StopMoving();
                }
            }
        }
    }

    public void UpdateDifficulty (DifficultyLevel difficulty) {
        if (!isGameOver) {
            if (currentDifficulty.ComboCreationRate != difficulty.ComboCreationRate) {
                CancelInvoke("SpawnBlocks");

                float firstPlacementInterval = difficulty.ComboCreationRate;
                if (lastCreationTime != -1) {
                    firstPlacementInterval -= (Time.time - lastCreationTime);
                }

                InvokeRepeating("SpawnBlocks",firstPlacementInterval,difficulty.ComboCreationRate);
            }

            currentDifficulty = difficulty;

            for (int i = 0; i < transform.childCount; i++) {
                BlocksGroup combo = transform.GetChild(i).GetComponent<BlocksGroup>();
                if (combo) {
                    combo.SetSpeed(currentDifficulty.ComboSpeed);
                }
            }
        }
    }
}
